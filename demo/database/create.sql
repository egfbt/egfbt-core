CREATE DATABASE logs;
USE logs;

CREATE TABLE `application` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server` varchar(45) NOT NULL,
  `application` varchar(45) NOT NULL,
  `environment` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `entry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logger` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp(),
  `details` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `level` (
  `id` int(11) NOT NULL,
  `level` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `logger` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `application` int(11) NOT NULL,
  `section` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `application` int(11) DEFAULT NULL,
  `subsystem` varchar(45) NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO level (id, level) VALUES (1,'debug'), (3, 'info'), (5, 'log'), (7, 'warn'), (9, 'critical');

grant SELECT, INSERT, UPDATE ON logs.* TO 'logs'@'%' IDENTIFIED BY 'logPass!23';
flush privileges;
