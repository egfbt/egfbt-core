module.exports = {

    application: {
        name: 'myApplication',
        //Note if this is not here look for a package.json file to load.
    },
    loggers: [
         {type: 'console', level: 'INFO', intercept: true},
         {type: 'file', level: 'DEBUG', config: {maxFiles: 3, name: 'application'}},
    ],
    drivers: [
        {type: 'directory', path: './drivers', recursive: true},
        {name: 'name', factory: (config) => {return new driver()}}
    ],
    resources: [
        {type: 'type', config: {configuration} },
        {type: 'type', config: {configuration} },
        {type: 'type', config: {configuration} },
        {type: 'type', config: {configuration} },
    ]


}