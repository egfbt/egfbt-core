const axios = require('axios');


class CentralAuthClient {
	constructor(name, cfg){
		this.name = name;
		if(cfg.url == undefined && cfg.api != undefined) cfg.url = cfg.api;
		if(cfg.timeout == undefined) cfg.timeout = 10000;
		this.api = axios.create({
			baseURL: cfg.url,
			timeout: cfg.timeout,
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
			}		
		});
		this.users = {};
	}

	async validateUser(userToken, ipAddress, username, security ){
		if(security === undefined || security === null) return true;
		var user = this.getUser(userToken, ipAddress, username);
		if(!user){
			return false;
		}
		if(ipAdress != user.ipAddress) {
			return false;
		}
		if(this.checkPermissions(user.perm, security)) return true;
		user = this.getUser(userToken, ipAddress, username, true);
		return this.checkPermissions(user.perm, security);
	}

	/**
	 * @param {*} token 
	 * @param {*} ipAddress 
	 * @param {*} username 
	 * @param {*} refresh 
	 */
	async getUser(token, ipAddress, username, refresh = false){
		if(!username || !token){
			return null;
		}
		if(refresh) this.users[token] = undefined;
		if(this.users[token] != undefined){
			var user = this.users[token];
			if(user.username.toLowerCase() == username.toLowerCase()){
				return user;
			}
		}

		var resp = await this.api.post('checkToken', {username, token, ipAddress}).catch(err => { throw err; });
		if(resp.data.authed){
			this.users[token] = resp.data;
			return resp.data;
		}
		else return null;
	}

	checkPermissions (userPerms, testPerms) {
		if(testPerms === undefined || testPerms === null) return true; //No security setup.
		if(typeof testPerms == 'string') testPerms = [testPerms];
		if(!Array.isArray(testPerms)) return true;	
		if(!Array.isArray(userPerms)) return false;
		if(testPerms.length == 0) return true;
		for(var i = 0; i < testPerms.length; i++ ){
			var perm = testPerms[i];
			if(userPerms.includes(perm)){
				return true;
			}
		};
		return false;
	}

	async close(){

	}
}

exports.getPool = (name, cfg) => {
	return new CentralAuthClient(name, cfg);
}


