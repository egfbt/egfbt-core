const base = require('../poolman/baseDB');
const ODBC = require( "mssql");

class mssql extends base {
 
	constructor(name, config, pool) {
		super(name, config);
		this.pool = pool;
	}
	
	async query(query, params = []) {
		if(params.length > 0) {
		    throw new Error('Check your query, this does not currently support pushed variables');
		} 
		try {
			const x = await this.pool.request().query(query);
			//--
			if(x.rowsAffected == 0) {
			   return [];
			} else {
			   return x.recordsets[0];
			}
		}
		catch(err) {
			//await this.reset();
			return [{"err": err}];
			//throw err;
		}
		// if(x.recordset == undefined){
		// 	console.warn('Unexpected result from query, please check formatting', x);
		// 	throw new Error('Unfulfilled query');
		// }
	}

	request() {
		var req = this.pool.request();
		return req;
	}

	async reset() {
		console.log("reset connection");
		this.pool.close();
		ODBC.close();
		this.pool = await fetchPool(this.config);
	}

	async close() {
		this.pool.close();
	}
}

function fetchPool(cfg, callback) {
	if(!cfg.port) cfg.port = 1433;

	const config = {
		user: cfg.user,
		password: cfg.password,
		server: cfg.server,
		port: cfg.port,
//		domain: cfg.domain,
		database: cfg.database,
		pool: cfg.pool
	}
	
    let pool = null;
    if (ODBC.ConnectionPool) {
        pool = ODBC.ConnectionPool;
    } else {
        pool = ODBC.Connection;
	}

    let p = new pool(config, function(err) {
        if(err) {
		   callback("error");
        } else {
           callback(p);
        }
	});
	
}

exports.getPool = async(name, cfg) => {
    let promise = new Promise(function(resolve, reject) {
        try {
          fetchPool(cfg, function(p) {
            if (p == "connection failed") {
               reject(p);
            } else {
               resolve(new mssql(name, cfg, p));
            }  
          });
        } catch(e) {
          reject("connection failed");
        }
    });
    return promise; 
}