const DRIVER = require('./drivers/Driver');


const register = new Map();


export const registerDriver = (name, factory) =>{

}

export const setRegistryMap = (config) => {

}

export const setResourceMap = (config) => {

}


export const getResource = (name) => {

}

export const getPool = (name) => {

}

const domains = new Map;
const PATH = require('path');
const FS = require('fs');

var domainPath = null;

const setDomainPath = (path) => {
    domainPath = path;
}

const getDomainSource = (name) => {
    var ename = 'egfbt-dom-' + name;
    if(!domainPath) return ename;
    var p = PATH.join(domainPath, name + '.js');
    if(FS.existsSync(p)) return p;
    return ename;
}


const getDomain = async(name) => {
    name = + name.replace('egfbt-dom-','');
    if(!domains.has(name)){
        var source = getDomainSource(name);    
        domains.set(name, await require(source).getDomain());
    } 
    return domains.get(name);        
}

module.exports = {
    getDomain,
    setDomainPath
}