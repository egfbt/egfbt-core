/**
 * Driver allows you to build a custom driver.
 * Note: that it is intended to override the factory function and the release function as needed.
 */
export default class Driver{

    constructor(environment = {}){
        this.environment = environment;
        if(environment.factory != undefined){
            this.factory = environment.factory;
        }
    }

    getInstance(config){

        return this.factory(config, this.environment);

    }

    releaseInstance(instance){

        return release(instance);

    }

    /**
     * Override this to cleanup, not that environment is passed in from the constructor
     * @param {*} config 
     * @param {*} environment 
     */
    factory(config, environment){
        throw new Error('factory(config) should be overloaded, base getInstance called');
    }

    /**
     * Override this to clean up the instance.
     * @param {*} instance 
     */
    release(instance) {

    }


}