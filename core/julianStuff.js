
//This should be moved to a new library.
const dateFns = require( "date-fns" );

exports.formatDate = ( date, format ) => {
	return dateFns.format( dateFns.parse( date ), format );
};

exports.toJulian = ( date ) => {
	return parseAndFormat ( date, "YYYYDDDD" );
};

exports.julianToDateObj = (julian) => {
	var sdate = julian + '';
	var year = sdate.substring(0,4);
	var days = sdate.substring(4);
	var date = new Date(year,0,0);
	date = dateFns.addDays(date, days);
	return date;
}

exports.julianToeStr = (julian, format='MM/DD/YYYY') => {
	var date = this.julianToDateObj(julian);
	return this.formatDate(date, format);
}

