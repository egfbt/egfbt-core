import Driver from './Driver';
const FS = require('fs');
const PATH = require('path');



const driverMap = new Map();


const loadDriversByDirectory = (prefix, path) => {
    if(! FS.existsSync(path)){
        throw new Error(`Path: ${path} does not exist, failed to load drivers`);
    }
    if(!FS.lstatSync(path).isDirectory()){
        throw new Error(`Path: ${path} is not a directory, failed to load drivers`);
    }

    const files = FS.readdirSync(path);
    for(var i = 0; i < files.length;i++){
        if(FS.lstatSync(files[i]).isDirectory()){
            loadDriversByDirectory(`${prefix}${files[i]}.`,PATH.join(path, files[i]))
        }
        else{
            const file = require(PATH.resolve(path, files[i]));
            CRAP MONKEYS, a class is a funciton, not an instance

        }
    }


        if(stat.isDirectory()){
            const fdir = FS.readdirSync(path);
            for(var i = 0; i < fdir.length;i++){
                var p = (pos != '') ? pos + '.':'';
                loadSettings(p + fdir[i], path + '/' + fdir[i]);
            }
            return;
        }
        var elem = require(PATH.resolve(path));
        var parts = pos.split('.').reverse();
        for(var i = 1; i < parts.length;i++){
            var sub = elem;
            elem = {};
            elem[parts[i]] = sub;
        }
        config = deepMerge(config, elem);
    }
    

}

export const registerDriver = (name, driver) => {
    if(driver instanceof Driver) {
        CRAP MONKEYS, a class is a funciton, not an instance, I should have noticed this...this...this actually tweaks my holw "driver plan anyways", a driver should be an instanceof a driver
            So it cannot be a driver. > hmm
        driverMap.set(name, driver);
    }
    else if(typeof driver == 'function'){
        driverMap.set(name, new Driver({factory:driver}));
    }
    else if(driver.factory != undefined){
        driverMap.set(name, new Driver(driver));
    }
}

/*  TODO evaluation of weather to keep this or not.
export const registerDriver = (name, driver) => {

    else if(typeof driver.type == 'string' && driver.type.toUpperCase() == 'DIRECTORY'){
        loadDriversByDirectory(name + '.', driver.path);
    }

    else if(typeof driver == 'object') {
        const keys = Object.keys(driver);
        for(var i = 0; i < keys.length; i++){

        }
        //Assume object structure. note name.sub formatting.
    }
}


const buildDriverListFromObject = (obj) => {
    const keys = Object.keys(obj);
    var list = [];

    Need to handle it if it is also a type directory. instead of a factory
    if it is a class Driver vs a function.  (Default Driver type)


    for(var i = 0; i< keys.length;i++){
        list.push( {
            name: keys[i],
            factory: obj[keys[i]]
        } );
    }
    return list;
}
*/


export const loadDrivers = (drivers) => {
    //Add default drivers, note that the end user can override these
    registerDriver('consoleLogger', require('./drivers/ConsoleLogFactory'));
    registerDriver('fileLogger', require('./drivers/FileLogFactory'));

    if(drivers == undefined) return; //No Drivers configured.

    if(typeof drivers.type == 'string' && drivers.type.toUpperCase() == 'DIRECTORY'){
        loadDriversByDirectory('', drivers.path);
    }
    else throw new Error(`Currently configuration requires drivers: {type: 'directory', path: path}`);
    
}