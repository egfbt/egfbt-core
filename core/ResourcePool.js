
export default class ResourcePool{

    constructor(config){
        //Configuration is expected to have a name, pool min and max, etc. also usage timeout.
        this.name = name;
        this.environment = environment;
    }

    get(config){

        return this.getInstance(config, this.environment);

    }

    getInstance(config, environment){
        throw new Error('getInstance(config) should be overloaded, base getInstance called');
    }

    releaseInstance(inst){
        //Not that this should be injected into the instance.
    }


}


const CONFIG = require('../config');

const base = require('./baseDB');
const {sleep} = require('../core');

var drivers = {};
var pools = {}; 
var lock = false;
var configRoot = 'resources';

exports.baseDB = base;

const registerDriver = (name, driver) => {
	drivers[name] = driver;
}


exports.registerDriver = registerDriver;

exports.setConfigRoot = (root) => {
	configRoot = root;
}

const getPool = async(poolName, cfg = null) => {
	while(lock) {
		await sleep(10);
	}
	if(pools[poolName]){
		return pools[poolName].pool;			
	}

	try{
		lock = true;
		if(!cfg){
			var cPath = (configRoot == '')? poolName: configRoot + '.' + poolName;
			cfg = CONFIG.getSettings(cPath);
		}
		if(!cfg){
			throw new Error(`Could not identify configuration for ${poolName}`);
		}
		if(!cfg.type) throw new Error(`Could not identify type for ${poolName}`)
		var driver = drivers[cfg.type];
		if(driver == undefined) throw new Error(`Driver not loaded ${cfg.type} for ${poolName}`);

		var pool = await driver.getPool(poolName, cfg);
		pools[poolName] = {pool}
		return pool;
	}
	catch(err){
		console.error(`Error trying to connect to ${poolName}:`);
		console.log(err);
		throw err;
	}
	finally{
		lock = false;
	}
}

exports.getPool = getPool;

exports.query = async (poolName, query, params = []) => {
	var db = await getPool(poolName);
	return await db.query(query, params);	
}

exports.closeConnections = async () => {
	var keys = Object.keys(pools);
	for(var i = 0; i < keys.length; i++){
		var elem = keys[i];
		console.log(`Shutting down ${elem}`);
		try{
			await pools[elem].pool.close();
		}
		catch(err){
			console.log(pools[elem].pool);
			console.error(err);
		}

	};
}