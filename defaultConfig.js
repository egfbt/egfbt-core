const LOGCONST = require('../logger/constants');
const IP = require('ip');

module.exports = {
	system:{
		ipAddress: IP.address(),
		environment: 'unknown',
	},
	app:{
		name: 'Unconfigured',
	},
	loggers: [],
	resources: [],
};

