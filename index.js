const CONFIG = require('egfbt-config');

const DriverMan = require('./core/driverManager');




const loadConfiguration = (configFiles) => {
    if(typeof configFiles == 'string'){
        CONFIG.addConfig(configFiles);
    }
    else if(Array.isArray(configFiles)){
        for(var i = 0; i < configFiles.length; i++){
            CONFIG.addConfig(configFiles[i]);
        }
    }
    else{
        throw new Error('Configuration Loading not setup for: ', configFiles);
    }

}


const configureResources = (resources) => {
    if(resources == undefined) return; //No resources configured

    should handle sub objects, so if it does not specify type look for sub objects.

    const resourceList = (Array.isArray(drivers))? drivers: buildArrayFromKeys(resources);
}


const configure = (configFiles) => {
    loadConfiguration(configFiles);
    var config = CONFIG.getConfig();

    //Expected section Application, default usables


    //Expected Section drivers
    loadDrivers(config.drivers);

    //Expected Section Resources
    configureResources(config.resources);

    //Expected Section logging
    configureLoggers()

    //Expected Section Logging, default usuables
    if(config.logging != undefined){
        configureLogger
    }

    
    //Expected Section Resources

    module.exports = {

        application: {
            name: 'myApplication',
            //Note if this is not here look for a package.json file to load.
        },
        loggers: [
             {type: 'console', level: 'INFO', intercept: true},
             {type: 'file', level: 'DEBUG', config: {maxFiles: 3, name: 'application'}},
        ],
        drivers: [
            {type: 'directory', path: './drivers', recursive: true},
            {name: 'name', factory: (config) => {return new driver()}}
        ],
        resources: [
            {type: 'type', config: {configuration} },
            {type: 'type', config: {configuration} },
            {type: 'type', config: {configuration} },
            {type: 'type', config: {configuration} },
        ]
    
    
    }

    
}




module.exports ={
    Driver = require('./core/Driver'),
    configure,
    registerDriver,
}










// module.exports = {
// 	...CONFIG, //getSetting, setSetting, resetConfig, addConfig, *config
	
// }



// functions

// get -> gets a resource by configuration and driver type.

// register -> register a driver type, note that flag pool indicates it is a pool type and claim/release should be called.
// 	logger flag indicates the resource is a logger driver.


// //Register default pool managers.
// POOLMAN.registerDriver('mssql', require('./drivers/mssql'));

// /**
//  * @param {path} configPath (not required, nullable) - the path to the application specific config, default './config'
//  */
// const init = async (configPath = null) =>{
// 	CONFIG.resetConfig();
// 	CONFIG.addConfig(require('./defaultConfig.js'));

// 	if(configPath == null);//Do Nothing
// 	else if(Array.isArray(configPath)){
// 		for(var i = 0; i < configPath.length; i++){
// 			CONFIG.addConfigPath(configPath[i]);
// 		}
// 	}
// 	else{
// 		CONFIG.addConfigPath(configPath);
// 	}	

// 	await LOGGER.init();
// };

// const shutdown = async () => {
// 	//Turn off the database logger.
// 	//Shutdown the logger???
// 	systemConfig.logger.database.level = 0;
// 	await POOLMAN.closeConnections();
// }

// global.CORE = {
// 	...LOGGER,
// 	...CONFIG,
// 	...POOLMAN,
// 	...DOMAIN,
// 	...require('./core'),
// 	...require('./core/datefns'),
// 	init,
// 	shutdown 

// }

// module.exports = global.CORE;

