const CONFIG = require('../../config');
const assert = require('assert');

//setSetting, getSetting, resetConfig, loadConfig, *config
describe('Ensire that set config can set a deep value', () => {
    it('Should set a deep value', () =>{
        CONFIG.resetConfig();
        CONFIG.setSetting('test.deep.value', 'value');
        var tval =  {test:{deep:{value:'value'}}};
        assert.deepEqual(CONFIG.getConfig(), tval);
    });
    it('Should set a deep value without messing up existing', () =>{
        CONFIG.resetConfig();
        CONFIG.setSetting('test.deep.value', 'value');
        CONFIG.setSetting('test.deep.value2', 'value');
        CONFIG.setSetting('test.deep2.value', 'value');
        
        var tval =  {
            test:{
                deep:{
                    value:'value',
                    value2: 'value'
                },
                deep2:{
                    value:'value'
                }
            }
        };
        assert.deepEqual(CONFIG.getConfig(), tval);
    });
});




// describe('Ensure it loaded config files as expected', () =>{
// 	CORE.configure('Test', './test/environment');
// 	it('should have loaded logger.js config', () =>{
// 		assert.equal(systemConfig.logger.core, 'debug');
// 	});
// });

// describe('Ensure it loaded config files as expected', () =>{
// 	it('should have loaded config without logger and err', () =>{
// 		CORE.configure('Test', './environment/');
// 		assert.equal(systemConfig.logger, undefined);
// 	});
// });

