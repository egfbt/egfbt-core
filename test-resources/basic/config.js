module.exports = {
	app: {
		name: 'Test2',
		aspect: 'therer',
	},
	logger: {
		file:{
			path: './logs'
		},
		email:{
			level: 0,
			target: 'scook@.com',
		},
		database:{
			level: 'DEBUG',		
			schema: 'dbo.log_',
			connection: {
				//server: '10.5.5.121', 
				server: "APPCLUSTER-DEV\\APPCLUSTERDEV",
//				database:'APPCLUSTERDEV',
				database: 'WES',
				domain: '.com',
				user: 'WES_WRITER',
				password: '',
				port: 1443,
				type: 'mssql',
				pool: {min: 1, max: 5}				
			},
		},
	}
};